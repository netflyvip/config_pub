


echo $*
echo $#
echo "*********************"
for arg in "$@"
do
    parameter=${arg%%=*}
    value=${arg#*=}
    case $parameter in
        user)
            user=$value
        ;;
        pwd)
            pwd=$value
        ;;
        server)
            server=$value
        ;;        
        -help)
            echo $usage
            exit 0
        ;;
        *)
            echo "${parameter} 非法变量"
            exit 1
        ;;
    esac
    echo $arg
    echo "变量：${parameter}"
    echo "值：${value}"
    echo '--------'
done


cd /root
rm -rf ServerStatus
mkdir ServerStatus
cd ServerStatus
wget --no-check-certificate -qO serverStatus_client.service 'https://gitlab.com/netflyvip/config_pub/-/raw/master/ServerStatus/serverStatus_client.service'
wget --no-check-certificate -qO client-linux.py 'https://gitlab.com/netflyvip/config_pub/-/raw/master/ServerStatus/client-linux.py'

USERNAMESTR="s/__USER__/$user/g"
PWDSTR="s/__PWD__/$pwd/g"
HOSTSTR="s/__SERVER__/$server/g"
sed -i $USERNAMESTR serverStatus_client.service
sed -i $PWDSTR serverStatus_client.service
sed -i $HOSTSTR serverStatus_client.service

rm -rf /lib/systemd/system/serverStatus_client.service
cp /root/ServerStatus/serverStatus_client.service /lib/systemd/system/serverStatus_client.service

systemctl enable serverStatus_client.service
# systemctl start app_api_service.service
systemctl restart serverStatus_client.service

systemctl status serverStatus_client.service
