import time

import boto3

regions = {
    'us-east-2': '美国-俄亥俄',
    'us-east-1': '美国-弗吉尼亚',
    'us-west-1': '美国-加利福尼亚',
    'us-west-2': '美国-俄勒冈',
    'af-south-1': '非洲-开普敦',
    'ap-south-1': '印度-孟买',
    'ap-northeast-3': '日本-大阪',
    'ap-northeast-2': '韩国-首尔',
    'ap-southeast-1': '新加坡',
    'ap-southeast-2': '澳洲-悉尼',
    'ap-northeast-1': '日本-东京',
    'ca-central-1': '加拿大',
    'ap-east-1': '中国-香港',
    'cn-north-1': '中国-北京',
    'cn-northwest-1': '中国-宁夏',
    'eu-central-1': '德国-法蘭克福',
    'eu-west-1': '愛爾蘭',
    'eu-west-2': '英国-倫敦',
    'eu-south-1': '意大利-米蘭',
    'eu-west-3': '法国-巴黎',
    'eu-north-1': '瑞典-斯德哥爾摩',
    'me-south-1': '中东-巴林',
    'sa-east-1': '巴西-圣保罗',
}

images = {

  'af-south-1': 'ami-0e830ea7310d7c996',
  'ap-east-1': 'ami-0458d9c6d012b5432',
  'ap-northeast-1': 'ami-0fa72bd3d748a670f',
  'ap-northeast-2': 'ami-0e8d8cd87a89b16dc',
  'ap-northeast-3': 'ami-07f22ab1929e01ebd',
  'ap-south-1': 'ami-0b42b1842d94a64db',
  'ap-southeast-1': 'ami-0211d4d77072856f6',
  'ap-southeast-2': 'ami-01c840b88b5c5ccc8',
  'ap-southeast-3': 'ami-0bdf8dad17a2cd525',
  'ca-central-1': 'ami-0a90b107128990b75',
  'eu-central-1': 'ami-049ed5fa529109ac4',
  'eu-north-1': 'ami-003a21f5bb59378bf',
  'eu-south-1': 'ami-033d51f7c23a632b0',
  'eu-west-1': 'ami-0ad0923fe3bd430fa',
  'eu-west-2': 'ami-0ff6ab6f1655c6a0a',
  'eu-west-3': 'ami-049b128fc09970fa8',
  'me-south-1': 'ami-0a0cb978ad3dfc543',
  'sa-east-1': 'ami-0aab7a3c632e871a4',
  'us-east-1': 'ami-0aa6e9e11c6482f0a',
  'us-east-2': 'ami-0f2b58cdb443f0b76',
  'us-west-1': 'ami-0e9490b4112b79fad',
  'us-west-2': 'ami-01b290b93957fd408',
}

arm_images = {
  'af-south-1': 'ami-03cc083617fb15090',
  'ap-east-1': 'ami-016d938b82d310800',
  'ap-northeast-1': 'ami-0ef8264e1846d3e53',
  'ap-northeast-2': 'ami-0ca2db4a7e42767e8',
  'ap-northeast-3': 'ami-0038ecdb03814956b',
  'ap-south-1': 'ami-033e540a6e76366c1',
  'ap-southeast-1': 'ami-07fd801f6a977c8ca',
  'ap-southeast-2': 'ami-0953ecf17df9734b1',
  'ap-southeast-3': 'ami-07c050fd616f78701',
  'ca-central-1': 'ami-06631d36b770c1a7c',
  'eu-central-1': 'ami-00d7fdef029dcff23',
  'eu-north-1': 'ami-0ad7c421a112d4b88',
  'eu-south-1': 'ami-0631bec3835369186',
  'eu-west-1': 'ami-093feae7324f11335',
  'eu-west-2': 'ami-056f32239a4450117',
  'eu-west-3': 'ami-0d8c0a23d67ac0772',
  'me-south-1': 'ami-0489b32c5469858f3',
  'sa-east-1': 'ami-0b222fbaa3a232fe3',
  'us-east-1': 'ami-0ffd2fd6f1a81e491',
  'us-east-2': 'ami-089cc5022596d93cb',
  'us-west-1': 'ami-04d6e85fbe9bc2ec9',
  'us-west-2': 'ami-0910da7b01abc3da6',
}



class AwsApi():
    def __init__(self, key_id, key_secret):
        self.region = 'us-east-2'
        self.key_id = key_id
        self.key_secret = key_secret

    def start(self, name='ec2'):
        self.client = boto3.client(name, region_name=self.region, aws_access_key_id=self.key_id,
                                   aws_secret_access_key=self.key_secret)

    # 查询配额
    def get_service_quota(self):
        try:
            self.region = 'us-west-2'
            self.start('service-quotas')
            ret = self.client.get_service_quota(ServiceCode='ec2', QuotaCode='L-1216C47A')
            # ret = aApi.client.list_aws_default_service_quotas(ServiceCode='ec2')
            print(f'当前配额： {int(ret['Quota']['Value'])}')
            return True
        except BaseException as e:
            print(f'配额查询失败 {e}')
            return False

    # 获取全部地区
    def get_describe_regions(self):
        try:
            self.region = 'us-east-2'
            self.start()
            response = self.client.describe_regions()
            text = ''
            self.region_list = []
            for region in response['Regions']:
                # print(region)
                region = region['RegionName']
                text += f'{region} ---- {regions.get(region, region)}\n'
                self.region_list.append(region)
            self.region_text = text
            return True
        except BaseException as e:
            self.region_text = f'获取地区列表失败 {e}'
            return False


    # ec2 查询安全组, 获取默认安全组
    def ec2_describe_default_security_groups(self, name='default'):
        try:
            response = self.client.describe_security_groups(
                GroupNames=[name]
            )
            self.GroupId = response['SecurityGroups'][0]['GroupId']
            self.ec2_authorize_security_group_ingress(self.GroupId)
            return True
        except BaseException as e:
            print(e)
            return False

    # 添加安全组规则
    def ec2_authorize_security_group_ingress(self, GroupID=''):
        try:
            self.client.authorize_security_group_ingress(
                GroupId=GroupID,
                IpPermissions=[
                    {
                        'IpProtocol': '-1',
                        'IpRanges': [
                            {
                                'CidrIp': '0.0.0.0/0',
                                'Description': 'string'
                            },
                        ],
                    },
                ]
            )
            return True
        except:
            return False

    def ec2_create_instances(self, InstanceType='t2.micro', disk_size=8, _type='x86'):
        # 先获取默认安全组
        try:
            self.ec2_describe_default_security_groups()
            BlockDeviceMappings = [
                {
                    'DeviceName': f'/dev/sda1',
                    'Ebs': {
                        'VolumeSize': int(disk_size),
                        'DeleteOnTermination': True,
                        'VolumeType': 'gp2'
                    }
                }
            ]

            UserData = '''#!/bin/sh
sudo service iptables stop 2> /dev/null ; chkconfig iptables off 2> /dev/null ;
sudo sed -i.bak '/^SELINUX=/cSELINUX=disabled' /etc/sysconfig/selinux;
sudo sed -i.bak '/^SELINUX=/cSELINUX=disabled' /etc/selinux/config;
sudo setenforce 0;
echo root:amalio123 |sudo chpasswd root;
sudo sed -i 's/^#\?PermitRootLogin.*/PermitRootLogin yes/g' /etc/ssh/sshd_config;
sudo sed -i 's/^#\?PasswordAuthentication.*/PasswordAuthentication yes/g' /etc/ssh/sshd_config;
sudo service sshd restart;
'''
            if _type == 'arm':
                image_id = arm_images.get(self.region)
            else:
                image_id = images.get(self.region)
            response = self.client.run_instances(
                BlockDeviceMappings=BlockDeviceMappings,
                UserData=UserData,
                ImageId=image_id,
                InstanceType=InstanceType,
                MaxCount=1,
                MinCount=1,
                Monitoring={
                    'Enabled': False
                },
                SecurityGroupIds=[
                    self.GroupId,
                ]
            )
            self.response = response
            # 添加标签
            self.instance_id = response['Instances'][0]['InstanceId']
            return True
        except BaseException as e:
            print(str(e))
            self.error_msg = str(e)
            return False

    # 获取实例状态
    def get_instance(self, instance_id):
        try:
            self.start('ec2')
            ret = self.client.describe_instances(InstanceIds=[instance_id])['Reservations'][0]['Instances'][0]
            # print(ret)
            self.ip = ret['PublicIpAddress']
            if self.ip == '': return False
            self.instance_id = instance_id
            return True
        except BaseException as e:
            print(f'获取实例状态失败, 请重试 {e}')
            return False


def start():
    print('开机助手 v1.1 by: @qikao')
    print('购买AWS账号 @qikao')
    key_info = input('请输入API信息, 格式为 keyid|secret : ')
    if len(key_info.strip().split('|')) != 2:
        print('API信息输入错误')
        return False

    keyid, secret = key_info.strip().split('|')

    aApi = AwsApi(keyid, secret)
    print(f'当前操作的账号 {keyid} | {secret}')

    aApi.get_service_quota()
    if not aApi.get_describe_regions():
        print('获取地区列表失败， 无法进行开机')
        return False
    print(aApi.region_text)
    _region = input('请选择需要开机的区域， 例如： us-east-1 : ') or 'us-east-1'
    _region = _region.strip()
    if _region not in aApi.region_list:
        print(f'该账号不支持 {_region} 区域, 请选择重新操作。')
        return False

    _type = input('请选择需要的平台 ARM 或者 X86, 默认X86 : ') or 'X86'

    _type = _type.lower()

    instance_type = input('请输入需要创建的实例类型：默认 t2.micro  : ') or 't2.micro'

    instance_type = instance_type.strip()

    disk_size = input('请输入磁盘大小， 默认 8 : ') or '8'

    disk_size = int(disk_size)
    aApi.region = _region
    aApi.start()
    if not aApi.ec2_create_instances(instance_type, disk_size=disk_size, _type=_type):
        print('创建失败')
        return False

    print('========实例创建成功=========')
    for num in range(20):
        print(f'第 {num + 1} 次 更新实例状态')
        if aApi.get_instance(aApi.instance_id): break
        time.sleep(5)

    print('========实例信息如下=========')
    print(f'实例ID: {aApi.instance_id}, 实例IP: {aApi.ip}, 实例地区: {_region}({regions.get(_region)}), 机器平台: {_type}')
    return True


if __name__ == '__main__':
    start()
